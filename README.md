# Custom OpenStack Images

## Description
OpenStack images are built in pipelines and stored in local registries [here](https://gitlab.ics.muni.cz/cloud/g2/loci/-/tree/downstream) and [here](https://gitlab.ics.muni.cz/cloud/g2/openstack-helm-images/-/tree/downstream). OS and OpenStack versions are specified, but the images are identical to the official ones.

The abovementioned images can be customized in this repository. That enables our team to create unofficial patches.

Images are tagged with a timestamp and pipeline ID. Images are kept for two months (as a backup), and therefore multiple tags for one image exist.

## Usage (get image)
1) Find your image [here](https://gitlab.ics.muni.cz/cloud/g2/custom-openstack-images/container_registry).

2) List all image tags by clicking on the image name.

3) Choose the desired tag.

Note: Tag format is ```date```T```hour```-```pipelineID```

## Usage (customize image)
Images are built on top of local official versions. **Do not change the beginning of the Dockerfiles!**

1) Navigate to the folder of the desired image and write the change to the Dockerfile. This approach is based on [Loci recommendation](https://github.com/openstack/loci#customizing).

2) Commit.

Pipeline automates build. No action is needed.
